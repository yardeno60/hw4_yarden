#include "OutStream.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* c'tor of OutStream
input: none
output: none
*/
OutStream::OutStream()
{
	_writeFile = stdout;
}

/* d'tor of OutStream
input: none
output: none
*/
OutStream::~OutStream()
{
}

/* overload of operator << in case of string
input: string to print
output: same OutStream
*/
OutStream& OutStream::operator<<(const char *str)
{
	fprintf(this->_writeFile, str);
	return *this;
}

/* overload of operator << in case of number
input: number to print
output: same OutStream
*/
OutStream& OutStream::operator<<(int num)
{
	char buffer[50] = "";
	_itoa_s(num, buffer, 10);
	fprintf(this->_writeFile, buffer);
	return *this;
}

/* overload of operator << in case of endline
input: pointer of function to print
output: same OutStream
*/
OutStream& OutStream::operator<<(void(*pf)(FILE*))
{
	pf(this->_writeFile);
	return *this;
}

/* prints \n
input: file to print to
output: none
*/
void endline(FILE* stream)
{
	fprintf(stream, "\n");
}