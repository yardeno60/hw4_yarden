#include "Logger.h"
#include "OutStream.h"

/* c'tor of Logger
input: none
output: none
*/
Logger::Logger()
{
	os = *(new OutStream());
	setStartLine();
}

/* d'tor of Logger
input: none
output: none
*/
Logger::~Logger()
{
}

/* set the start of line in Logger
input: none
output: none
*/
void Logger::setStartLine()
{
	static unsigned int counter = 0;
	_startLine = true;
	if (counter)
	{
		this->os << counter;
	}
	++counter;
}

/* overload of operator << in case of looger and a string
input: logger and string to log
output: same logger
*/
Logger & operator<<(Logger & l, const char * msg)
{
	if (l._startLine)
	{
		l.os << " LOG ";
		l._startLine = false;
	}
	l.os << msg;
	return l;
}

/* overload of operator << in case of looger and a number
input: logger and number to log
output: same logger
*/
Logger & operator<<(Logger & l, int num)
{
	if (l._startLine)
	{
		l.os << " LOG ";
		l._startLine = false;
	}
	l.os << num;
	return l;
}

/* overload of operator << in case of looger and an endline
input: logger and pointer to function to log
output: same logger
*/
Logger & operator<<(Logger & l, void(*pf)(FILE*))
{
	l.os.operator<<(pf);
	l.setStartLine();
	return l;
}
