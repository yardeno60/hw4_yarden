#pragma once

#include "OutStream.h"
#include <string.h>
#include <stdlib.h>

#define MAX_ASCII 126
#define MIN_ASCII 32
#define MAX_SIZE 100

class OutStreamEncrypted : public OutStream
{
private:
	int _shift;
public:
	OutStreamEncrypted(int shift);
	~OutStreamEncrypted();
	OutStreamEncrypted& operator<<(const char *str);
	OutStreamEncrypted& operator<<(int num);
	OutStreamEncrypted& operator<<(void(*pf)(FILE*));

};
