#include "OutStreamEncrypted.h"

/* c'tor of OutStreamEncrypted
input: shift of the encryption
output: none
*/
OutStreamEncrypted::OutStreamEncrypted(int shift)
{
	this->_writeFile = stdout;
	this->_shift = shift;
}

/* d'tor of OutStreamEncrypted
input: none
output: none
*/
OutStreamEncrypted::~OutStreamEncrypted()
{
}

/* overload of operator << in case of string
input: string to encrypt and print
output: same OutStreamEncrypted
*/
OutStreamEncrypted & OutStreamEncrypted::operator<<(const char * str)
{
	int i = 0;
	char encrypted[MAX_SIZE] = "";
	int asciiLetter = 0;
	for (i = 0; i < strlen(str); ++i)
	{
		asciiLetter = int(str[i]);
		asciiLetter += this->_shift;
		if (asciiLetter > MAX_ASCII)
		{
			asciiLetter = asciiLetter % (MAX_ASCII + 1);
			asciiLetter += MIN_ASCII;
		}
		encrypted[i] = char(asciiLetter);
	}
	this->OutStream::operator<< (encrypted);
	return *this;
}

/* overload of operator << in case of number
input: number to encrypt and print
output: same OutStreamEncrypted
*/
OutStreamEncrypted & OutStreamEncrypted::operator<<(int num)
{
	char buffer[MAX_SIZE] = "";
	_itoa_s(num, buffer, 10);
	*this << buffer;
	return *this;
}

/* overload of operator << in case of endline
input: pointer of function to print
output: same OutStreamEncrypted
*/
OutStreamEncrypted& OutStreamEncrypted::operator<<(void(*pf)(FILE*))
{
	//inheritance won't work without this :(
	this->OutStream::operator<< (pf);
	return *this;
}