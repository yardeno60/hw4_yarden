#pragma once
#include <stdio.h>

class OutStream
{
protected:
	FILE * _writeFile;

public:

	OutStream();
	~OutStream();

	OutStream& operator<<(const char *str);
	OutStream& operator<<(int num);
	OutStream& operator<<(void(*pf)(FILE*));
};

void endline(FILE* stream);
