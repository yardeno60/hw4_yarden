#include "FileStream.h"

/* c'tor of FileStream
input: path to file
output: none
*/
FileStream::FileStream(char path[])
{
	fopen_s(&(this->_writeFile), path, "w");
}

/* d'tor of FileStream
input: none
output: none
*/
FileStream::~FileStream()
{
	fclose(this->_writeFile);
}
