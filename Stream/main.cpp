#include "OutStream.h"
#include "Logger.h"
#include "FileStream.h"
#include "OutStreamEncrypted.h"

int main(int argc, char **argv)
{
	OutStream screen;
	screen<<"I am the doctor and I'm ";
	screen << 1500;
	screen << " years old";
	screen << endline;

	FileStream file("lol.txt");
	file << "I am the doctor and I'm ";
	file << 1500;
	file << " years old";
	file << endline;

	OutStreamEncrypted cipher(3);
	cipher << "I am the doctor and I'm ";
	cipher << 1500;
	cipher << " years old";
	cipher << endline;

	Logger l1;
	Logger l2;
	l1 << "I am the doctor and I'm ";
	l1 << 1500;
	l1 << " years old";
	l1 << endline;
	l2 << "hello";
	l2 << endline;
	l1 << "test";

	getchar();
	return 0;
}
